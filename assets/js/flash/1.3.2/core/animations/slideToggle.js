flashCore.prototype.slideToggle = function(el, timing, callback) {
    var self = this;

    var el_max_height = 0;
    if(el.style.maxHeight.replace('px', '').replace('%', '') === '0' || el.style.maxHeight.replace('px', '').replace('%', '') === '') {
        self.slideDown(el, timing, callback);
    } else {
        self.slideUp(el, timing, callback);
    }
}